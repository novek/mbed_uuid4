/**
 * Copyright (c) 2018 rxi
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the MIT license. See LICENSE for details.
 */

#ifndef MBED_UUID4_H
#define MBED_UUID4_H

#define USE_MBED_TLS 1

#include <stdio.h>
#include <stdint.h>

#if USE_MBED_TLS
#include "mbedtls/entropy.h"
#elif defined(_WIN32)
#include <windows.h>
#include <wincrypt.h>
#endif



#define UUID4_VERSION "1.0.0"
#define UUID4_LEN 37


#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  UUID4_ESUCCESS =  0,
  UUID4_EFAILURE = -1
} uuid4_error_t;

uuid4_error_t  uuid4_init();
uuid4_error_t uuid4_generate(char *dst);

#ifdef __cplusplus
}
#endif

#endif
