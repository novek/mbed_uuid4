#include "uuid4.h"

#define DEBUG 1

#if defined(DEBUG)
#define DBG printf
#else 
#define DGB(x)
#endif

static uint64_t seed[2];

static uint64_t xorshift128plus(uint64_t *s) {
  /* http://xorshift.di.unimi.it/xorshift128plus.c */
  uint64_t s1 = s[0];
  const uint64_t s0 = s[1];
  s[0] = s0;
  s1 ^= s1 << 23;
  s[1] = s1 ^ s0 ^ (s1 >> 18) ^ (s0 >> 5);
  return s[1] + s0;
}


uuid4_error_t uuid4_init() {
  #if USE_MBED_TLS
  mbedtls_entropy_context entropy;
  mbedtls_entropy_init( &entropy );
  uint64_t r0;
  uint64_t r1;
  int ret = mbedtls_entropy_func( &entropy, &r0, sizeof(r0) );
  ret = mbedtls_entropy_func( &entropy, &r1, sizeof(r1) );
  mbedtls_entropy_free( &entropy );
  if( ret != 0 )
  {
    DBG("Error mbedtls_entropy_func %d ",ret);
    return UUID4_EFAILURE;
  }
  seed[0]=r0;
  seed[1]=r1;
  DBG("seed[0]-0x%jx  seed[1]-0x%jx\r\n", seed[0],seed[1]);
#elif defined(__linux__) || defined(__APPLE__) || defined(__FreeBSD__)
  int res;
  FILE *fp = fopen("/dev/urandom", "rb");
  if (!fp) {
    return UUID4_EFAILURE;
  }
  res = fread(seed, 1, sizeof(seed), fp);
  fclose(fp);
  if ( res != sizeof(seed) ) {
    return UUID4_EFAILURE;
  }

#elif defined(_WIN32)
  int res;
  HCRYPTPROV hCryptProv;
  res = CryptAcquireContext(
    &hCryptProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);
  if (!res) {
    return UUID4_EFAILURE;
  }
  res = CryptGenRandom(hCryptProv, (DWORD) sizeof(seed), (PBYTE) seed);
  CryptReleaseContext(hCryptProv, 0);
  if (!res) {
    return UUID4_EFAILURE;
  }
#else
  #error "unsupported platform"
#endif
  return UUID4_ESUCCESS;
}


uuid4_error_t uuid4_generate(char *dst) {
  static const char *template = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx";
  static const char *chars = "0123456789abcdef";
  union { unsigned char b[16]; uint64_t word[2]; } s;
  const char *p;
  int i, n;
  /* get random */
  s.word[0] = xorshift128plus(seed);
  s.word[1] = xorshift128plus(seed);
  /* build string */
  p = template;
  i = 0;
  while (*p) {
    n = s.b[i >> 1];
    n = (i & 1) ? (n >> 4) : (n & 0xf);
    switch (*p) {
      case 'x'  : *dst = chars[n];              i++;  break;
      case 'y'  : *dst = chars[(n & 0x3) + 8];  i++;  break;
      default   : *dst = *p;
    }
    dst++, p++;
  }
  *dst = '\0';
  return UUID4_ESUCCESS;
}
