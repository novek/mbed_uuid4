# MBED UUID4
**A fork of [https://github.com/rxi/uuid4](https://github.com/rxi/uuid4)**

A tiny C/Cpp library for generating [uuid4](http://www.ietf.org/rfc/rfc4122.txt)
strings.

It requires [Mbed TLS](https://github.com/ARMmbed/mbedtls) for generating the seed, if not using with Mbed TLS , set `#define USE_MBED_TLS 1` to `0` in [uuid.h](./uuid4.h)

### Usage
[uuid4.c](./uuid4.c) and [uuid4.h](./uuid4.h) should be
dropped into an existing project. The library contains two functions:
one to initialize the library and one to generate a uuid4 string and
store it in a buffer of at least `UUID4_LEN` in size.

```c
char buf[UUID4_LEN];
uuid4_init();
uuid4_generate(buf);
printf("%s\r\n", buf);
```

`uuid4_generate()` is not thread-safe.

### License
This library is free software; you can redistribute it and/or modify it
under the terms of the MIT license.
